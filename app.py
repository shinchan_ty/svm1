from flask import Flask, request, jsonify, render_template
from PIL import Image
import pickle
import io
import base64
import numpy as np
import os
from img2vec_pytorch import Img2Vec

app = Flask(__name__)

# Initialize global variables
model = None
img2vec = None

def load_model():
    global model
    try:
        with open('svm_model.pkl', 'rb') as model_file:
            model = pickle.load(model_file)
        app.logger.info("Model loaded successfully.")
    except Exception as e:
        app.logger.error(f"Error loading model: {e}")

def initialize_img2vec():
    global img2vec
    try:
        img2vec = Img2Vec(model='resnet-18')
        app.logger.info("img2vec model initialized successfully.")
    except Exception as e:
        app.logger.error(f"Error initializing img2vec model: {e}")

# Load the SVM model and img2vec model when the app starts
load_model()
initialize_img2vec()

# Define age ranges
age_ranges = ['11-18', '19-26', '27-34', '35-42', '43-50', '51-58', 'lesser_than_11', 'greater_than_58', 'Not_HumanFace']

def preprocess_image(image):
    image = image.resize((264, 264))
    image = np.array(image)
    if image.shape[-1] == 4:  # Convert RGBA to RGB
        image = image[..., :3]
    return image

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/camera')
def camera():
    return render_template('camera.html')

@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/index')
def home_page():
    return render_template('index.html')

@app.route('/predict-age', methods=['POST'])
def predict_age():
    global model
    if model is None:
        app.logger.error("Model is not loaded.")
        return jsonify({'error': 'Model is not loaded.'}), 500

    try:
        data = request.get_json()
        app.logger.info(f"Received data: {data}")

        if not data:
            return jsonify({'error': 'No data provided'}), 400
        
        if 'image' not in data:
            app.logger.error(f"Missing 'image' key in data: {data}")
            return jsonify({'error': 'Missing image key in data'}), 400

        image_data = data['image'].split(',')[1]  # Get base64 part of the image

        image = Image.open(io.BytesIO(base64.b64decode(image_data)))

        # Preprocess the image
        processed_image = preprocess_image(image)
        
        # Extract features using img2vec model
        features = img2vec.get_vec(Image.fromarray(processed_image))
        
        # Make prediction
        features = features.reshape(1, -1)  # Reshape for the SVM model
        prediction = model.predict(features)
        predicted_age_range = age_ranges[prediction[0]]

        return jsonify({'predicted_age_range': predicted_age_range})

    except Exception as e:
        app.logger.error(f"Error occurred: {e}")
        return jsonify({'error': 'An internal error occurred'}), 500

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)