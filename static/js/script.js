
document.getElementById('toggleNavbar').addEventListener('click', function () {
    const sideMenu = document.getElementById('sideMenu');
    sideMenu.classList.toggle('hidden');
    sideMenu.classList.toggle('open');
});

document.getElementById('closeMenu').addEventListener('click', function () {
    const sideMenu = document.getElementById('sideMenu');
    sideMenu.classList.add('hidden');
    sideMenu.classList.remove
}) 


document.addEventListener('DOMContentLoaded', () => {
    const video = document.createElement('video');
    const takePhotoBtn = document.getElementById('take-photo-btn');
    const uploadPhotoInput = document.getElementById('upload-photo-input');
    const result = document.createElement('p');

    document.body.appendChild(video);
    document.body.appendChild(result);

    // Access the webcam
    navigator.mediaDevices.getUserMedia({ video: true })
        .then(stream => {
            video.srcObject = stream;
            video.play();
        })
        .catch(err => {
            console.error('Error accessing webcam: ', err);
        });

    takePhotoBtn.addEventListener('click', () => {
        captureAndPredict(video, result);
    });

    uploadPhotoInput.addEventListener('change', (event) => {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = () => {
                const imageUrl = reader.result;
                predictFromImageUrl(imageUrl, result);
            };
            reader.readAsDataURL(file);
        }
    });
    });

    function captureAndPredict(video, resultElement) {
        const canvas = document.createElement('canvas');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        const context = canvas.getContext('2d');
        context.drawImage(video, 0, 0, canvas.width, canvas.height);

        const imageDataUrl = canvas.toDataURL('image/jpeg');
        sendImageForPrediction(imageDataUrl, resultElement);
    }

    function predictFromImageUrl(imageUrl, resultElement) {
        sendImageForPrediction(imageUrl, resultElement);
    }

    function sendImageToServer(base64Image) {
        fetch('http://127.0.0.1:5000/predict-age', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                image: base64Image
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            if (data.predicted_age !== undefined) {
                alert(`Predicted Age: ${data.predicted_age}`);
            } else {
                alert('Predicted age is undefined');
            }
        })
        .catch(error => console.error('Error:', error));
    }
    
    function convertImageToBase64AndSend(file) {
        const reader = new FileReader();
        reader.onloadend = () => {
            const base64Image = reader.result;
            sendImageToServer(base64Image);
        };
        reader.readAsDataURL(file);
    }
    
    document.getElementById('imageInput').addEventListener('change', function() {
        const file = this.files[0];
        if (file) {
            convertImageToBase64AndSend(file);
        }
    });

     